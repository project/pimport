<?php

/**
 * @file
 * Handles adding, editing, cloning CSV profiles.
 *
 * @ingroup pimport_ui
 * @{
 */

/**
 * Adds a new profile configuration.
 *
 * @see pimport_ui_add_validate()
 * @see pimport_ui_add_submit()
 */
function pimport_ui_add(&$form_state) {
  $form['settings']['#tree'] = TRUE;
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Please enter a descriptive title for this profile.'),
    '#required' => TRUE,
    '#weight' => -10,
  );
  $form['explanation'] = array(
    '#type' => 'textarea',
    '#title' => t("Description"),
    '#description' => t("Optional - a brief description of this profile."),
    '#weight' => -9,
  );
  $node_type_options = _pimport_content_type_options(); 
  if (!empty($node_type_options)) {
    $form['types'] = array(
      '#type' => 'select',
      '#title' => t('Node type associations'),
      '#description' => t('Please select which node types this profile can be used with.'),
      '#options' => $node_type_options,
      '#multiple' => TRUE,
      '#weight' => -6,
    );
  }
  else {
    $form['no_types'] = array(
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#value' => t('Pimport is not enabled for any node types. !settings_link
        <br />You can save the profile anyway and edit the profile later to add node type associations ',
         array('!settings_link' => l('Edit Pimport settings.', 'admin/settings/pimport') ) ),
      '#suffix' => '</p>',
    );
  }
  $form['settings']['date_format'] = array(
    '#type' => 'select',
    '#title' => t('Date format'),
    '#description' => t('Optional - the date format used for importing into this profile.'),
    '#options' => pimport_date_formats(),
    '#weight' => -4,
  );
  $form['settings']['bounce'] = array(
    '#type' => 'checkbox',
    '#title' => t('Skip rows without unique ID'),
    '#description' => t("Reject rows in import CSV which don't contain the specified unique ID fields. Useful for when some fields contain comments."),
    '#weight' => -2,
  );
  $form['settings']['owner_type'] = array(
    '#type' => 'select',
    '#title' => t('Allocate to referenced user (optional)'),
    '#description' => t('Select how the user will be referenced.'),
    '#options' => array(
      NULL => t("Don't use"),
      'uid' => t('User ID'),
      'username' => t('User Name'),
      'email' => t('Email Address'),
    ),
    '#weight' => 0,
  );
  $form['settings']['owner_fallback'] = array(
    '#type' => 'textfield',
    '#title' => t("Provide a default fallback user"),
    '#description' => t("To be used in conjunction with user reference above. If omitted the importing user will be the node owner."),
    '#autocomplete_path' => 'user/autocomplete',
    '#weight' => 0,
  );
  return confirm_form($form, t('Create New Profile'), 'admin/settings/pimport/pimport_ui', t('Please click the confirmation button to save your new profile.'));
} // pimport_ui_add

/**
 * Validation callback for pimport_ui_add form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_add()
 * @see pimport_ui_add_submit()
 */
function pimport_ui_add_validate(&$form, &$form_state) {
  if (!preg_match("/^[0-9a-zA-Z._\- ]+$/", $form_state['values']['title'])) {
    form_set_error('title', t("Error: Profile title field contains invalid characters."));
  }
  if (db_result(db_query("SELECT pid FROM {pimport_profiles} WHERE title = '%s'", $form_state['values']['title']))) {
    form_set_error('title', t("Error: Profile title is not unique."));
  }
  if (!empty($form_state['values']['settings']['owner_type']) && ($name = $form_state['values']['settings']['owner_fallback']) && !($account = user_load(array('name' => $name)))) {
    form_set_error('settings][owner_fallback', t("Error: Please give a valid username."));
  }
  else {
    form_set_value(array('#parents' => array('settings', 'owner_fallback')), $account->uid, $form_state);
  }
} // pimport_ui_add_validate

/**
 * Submit callback for pimport_ui_add form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_add()
 * @see pimport_ui_add_validate()
 */
function pimport_ui_add_submit($form, &$form_state, $update = array()) {
  if (_pimport_ui_operations_submit($form_state['values'])) {
    watchdog('pimport', "Profile '@profile' created.", array('@profile' => $form_state['values']['title']), WATCHDOG_INFO, l(t('view'), "admin/settings/pimport/manage/". $form_state['values']['pid']));
    drupal_set_message(t("Profile '@profile' created.", array('@profile' => $form_state['values']['title'])));
  }
  else {
    watchdog('pimport', "Error creating profile '@profile'.", array('@profile' => $form_state['values']['title']), WATCHDOG_ERROR);
    drupal_set_message(t('Profile not created. Please contact your <a href="mailto:!sysadmin">System Administrator</a>.', array('!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))), 'error');
  }
  $form_state['redirect'] = 'admin/settings/pimport/pimport_ui';
} // function pimport_ui_add_submit

/**
 * Edits a profile configuration.
 *
 * @param $pid
 *   An integer containing a reference to a profile
 */
function pimport_ui_edit(&$form_state, $profile) {
  $form['settings']['#tree'] = TRUE;
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Please enter a descriptive title for this profile.'),
    '#default_value' => $profile->title,
    '#required' => TRUE,
    '#weight' => -10,
  );
  $form['explanation'] = array(
    '#type' => 'textarea',
    '#title' => t("Description"),
    '#description' => t("Optional - a brief description of this profile."),
    '#default_value' => $profile->explanation,
    '#weight' => -9,
  );
  $form['types'] = array(
    '#type' => 'select',
    '#title' => t('Node type associations'),
    '#description' => t('Please select which node types this profile can be used with.'),
    '#options' => _pimport_content_type_options(),
    '#multiple' => TRUE,
    '#default_value' => pimport_profile_fetch_types($profile->pid),
    '#weight' => -6,
  );
  if (variable_get('pimport_use_permissions', FALSE) && user_access('administer permissions')) {
    $form['roles'] = array(
      '#type' => 'select',
      '#title' => t('User Permissions'),
      '#description' => t('User access to this profile, to alter please follow this link to the <a href="@url">Drupal permissions</a> page.<br /><strong>It is your responsibility to ensure you select a role you have access to, else you will not be able to access this profile after you click confirm.</strong>', array('@url' => url('admin/user/permissions', array('fragment' => 'module-pimport')))),
      '#options' => user_roles(),
      '#multiple' => TRUE,
      '#default_value' => array_flip(user_roles(FALSE, _pimport_perm_profiles($profile))),
      '#disabled' => TRUE,
      '#weight' => -5,
    );
  }
  $form['settings']['date_format'] = array(
    '#type' => 'select',
    '#title' => t('Date format'),
    '#description' => t('Optional - the date format used for importing into this profile.'),
    '#options' => pimport_date_formats(),
    '#default_value' => $profile->settings['date_format'],
    '#weight' => -4,
  );
  $form['settings']['bounce'] = array(
    '#type' => 'checkbox',
    '#title' => t('Skip rows without unique ID'),
    '#description' => t("Reject rows in import CSV which don't contain the specified unique ID fields. Useful for when some fields contain comments."),
    '#default_value' => $profile->settings['bounce'],
    '#weight' => -2,
  );
  $form['settings']['owner_type'] = array(
    '#type' => 'select',
    '#title' => t('Allocate to referenced user (optional)'),
    '#description' => t('Select how the user will be referenced.'),
    '#options' => array(
      NULL => t("Don't use"),
      'uid' => t('User ID'),
      'username' => t('User Name'),
      'email' => t('Email Address'),
    ),
    '#default_value' => $profile->settings['owner_type'],
    '#weight' => 0,
  );
  $form['settings']['owner_fallback'] = array(
    '#type' => 'textfield',
    '#title' => t("Provide a default fallback user"),
    '#description' => t("To be used in conjunction with user reference above. If omitted the importing user will be the node owner."),
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => user_load($profile->settings['owner_fallback'])->name,
    '#weight' => 0,
  );
  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $profile->pid,
  );
  $form['created'] = array(
    '#type' => 'hidden',
    '#value' => $profile->created,
  );
  return confirm_form($form, t("Edit Profile '@profile':", array('@profile' => $profile->title)), 'admin/settings/pimport/pimport_ui', t('Please click the confirmation button to save the changes to your profile.'));
} // pimport_ui_edit

/**
 * Validation callback for pimport_ui_edit form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_edit()
 * @see pimport_ui_edit_submit()
 */
function pimport_ui_edit_validate(&$form, &$form_state) {
  if (!preg_match("/^[0-9a-zA-Z._\- ]+$/", $form_state['values']['title'])) {
    form_set_error('title', t("Error: Profile Title field contains invalid characters."));
  }
  if (($pid = db_result(db_query("SELECT pid FROM {pimport_profiles} WHERE title = '%s'", $form_state['values']['title']))) && $pid != $form_state['values']['pid']) {
    form_set_error('title', t("Error: Profile Title is not unique."));
  }
  if (!empty($form_state['values']['settings']['owner_type']) && ($name = $form_state['values']['settings']['owner_fallback']) && !($account = user_load(array('name' => $name)))) {
    form_set_error('settings][owner_fallback', t("Error: Please give a valid username."));
  }
  else {
    form_set_value(array('#parents' => array('settings', 'owner_fallback')), $account->uid, $form_state);
  }
} // pimport_ui_edit_validate

/**
 * Submit callback for pimport_ui_edit form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_edit()
 * @see pimport_ui_edit_validate()
 */
function pimport_ui_edit_submit($form, &$form_state) {
  if (_pimport_ui_operations_submit($form_state['values'], 'pid')) {
    watchdog('pimport', "Profile '@profile' successfully altered.", array('@profile' => $form_state['values']['title']), WATCHDOG_INFO, l(t('view'), "admin/settings/pimport/manage/". $form_state['values']['pid']));
    drupal_set_message(t("Profile '@profile' successfully altered.", array('@profile' => $form_state['values']['title'])));
  }
  else {
    watchdog('pimport', "Error altering profile '@profile'.", array('@profile' => $form_state['values']['title']), WATCHDOG_ERROR, l(t('view'), "admin/settings/pimport/manage/". $form_state['values']['pid']));
    drupal_set_message(t("Error altering profile '@profile', please contact the <a href=\"mailto:!sysadmin\">System Administrator</a>.", array('@profile' => $form_state['values']['title'], '!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))), 'error');
  }
  $form_state['redirect'] = 'admin/settings/pimport/pimport_ui';
} // function pimport_ui_edit_submit

/**
 * Deletes a profile configuration.
 *
 * @param $form_state
 *   Drupal form_state array.
 * @param $profile
 *   pimport CSV profile object.
 *
 * @see pimport_ui_delete_submit()
 * @see theme_pimport_display()
 */
function pimport_ui_delete(&$form_state, $profile) {
  $form['display'] = array(
    '#type' => 'markup',
    '#value' => theme('pimport_display', $profile),
  );
  $form['title'] = array(
    '#type' => 'hidden',
    '#value' => $profile->title,
  );
  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $profile->pid,
  );
  return confirm_form($form, t("Delete Profile '@profile'?", array('@profile' => $profile->title)), 'admin/settings/pimport/pimport_ui', t("<p>Caution this action is permanent and cannot be undone.</p><p>It will destroy all configuration for this profile.</p>"));
} // pimport_ui_delete

/**
 * Submit callback for pimport_ui_delete form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_delete()
 */
function pimport_ui_delete_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  if (db_query("DELETE FROM {pimport_profiles} WHERE pid = %d", $form_values['pid']) && db_query("DELETE FROM {pimport_node_type_association} WHERE pid = %d", $form_values['pid']) && db_query("DELETE FROM {pimport_profile_data} WHERE pid = %d", $form_values['pid'])) {
    watchdog('pimport', "Profile and profile data successfully deleted from '@profile'.", array('@profile' => $form_values['title']), WATCHDOG_INFO);
    drupal_set_message(t("Profile and profile data successfully deleted from '@profile'.", array('@profile' => $form_values['title'])));
  }
  else {
    watchdog('pimport', "Error deleting profile '@profile'.", array('@profile' => $form_values['title']), WATCHDOG_ERROR, l(t('view'), "admin/settings/pimport/manage/". $form_values['pid']));
    drupal_set_message(t("Error deleting profile '@profile', please contact the <a href=\"mailto:!sysadmin\">System Administrator</a>.", array('@profile' => $form_values['title'], '!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))), 'error');
  }
  module_invoke_all('pimport_profile_delete', $form_values['pid']);
  $form_state['redirect'] = 'admin/settings/pimport/pimport_ui';
} // pimport_ui_delete_submit

/**
 * Clones a profile configuration.
 *
 * @param $form_state
 *   Drupal form_state array.
 * @param $profile
 *   pimport CSV profile object.
 *
 * @see pimport_ui_clone_validate()
 * @see pimport_ui_clone_submit()
 */
function pimport_ui_clone(&$form_state, $profile) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t("The human-readable name of this profile. It is recommended that this name begin with a capital letter and contain only alphanumeric characters. This name should be unique."),
    '#required' => TRUE,
    '#default_value' => sprintf("Copy of %s", $profile->title),
  );
  $form['explanation'] = array(
    '#type' => 'textfield',
    '#title' => t("Description"),
    '#description' => t("Optional - a brief description of this profile."),
    '#default_value' => $profile->explanation,
  );
  $form['old_pid'] = array(
    '#type' => 'hidden',
    '#value' => $profile->pid,
  );
  return confirm_form($form, t("Clone profile '@profile':", array('@profile' => $profile->title)), 'admin/settings/pimport/pimport_ui', ' ', t('Create'), t('Cancel'));
} // pimport_ui_clone

/**
 * Validation callback for pimport_ui_clone form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_clone()
 * @see pimport_ui_clone_submit()
 */
function pimport_ui_clone_validate(&$form, &$form_state) {
  if (!preg_match("/^[0-9a-zA-Z._\- ]+$/", $form_state['values']['title'])) {
    form_set_error('title', t("Error: Profile Title field contains invalid characters."));
  }
  if (db_result(db_query("SELECT pid FROM {pimport_profiles} WHERE title = '%s'", $form_state['values']['title']))) {
    form_set_error('title', t("Error: Profile Title is not unique."));
  }
} // pimport_ui_clone_validate

/**
 * Submit callback for pimport_ui_clone form.
 *
 * @TODO Make use of the _pimport_ui_operations_submit function.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_clone()
 * @see pimport_ui_clone_validate()
 */
function pimport_ui_clone_submit($form, &$form_state) {
  $form_state['values']['settings'] = unserialize(db_result(db_query("SELECT settings FROM {pimport_profiles} WHERE pid = %d", $form_state['values']['old_pid'])));
  $form_state['values']['types'] = pimport_profile_fetch_types($form_state['values']['old_pid']);
  if ($flag = _pimport_ui_operations_submit($form_state['values'])) {
    $query = db_query("SELECT * FROM {pimport_profile_data} WHERE pid = %d", $form_state['values']['old_pid']);
    while ($fetch = db_fetch_object($query)) {
      $fetch->pid = $form_state['values']['pid'];
      if (!drupal_write_record('pimport_profile_data', $fetch)) {
        $flag = FALSE;
      }
    }
  }
  if ($flag) {
    drupal_set_message(t("Profile '@profile' successfully created.", array('@profile' => $form_state['values']['title'])));
  }
  else {
    drupal_set_message(t("Error cloning profile '@profile'. Please contact your <a href=\"mailto:!sysadmin\">System Administrator</a>.", array('@profile' => $form_state['values']['title'], '!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))));
  }
  $form_state['redirect'] = 'admin/settings/pimport/pimport_ui';
} // pimport_ui_clone_submit

/**
 * Generic helper function for writing/ updating CSV profiles.
 */
function _pimport_ui_operations_submit(&$form_values, $update = array()) {
  global $user;
  $time = time();
  $form_values['uid'] = $user->uid;
  $form_values['settings'] = serialize($form_values['settings']);
  if (empty($form_values['created'])) {
    $form_values['created'] = $time;
  }
  $form_values['changed'] = $time;
  if (!drupal_write_record('pimport_profiles', $form_values, $update)) {
    return FALSE;
  }
  if (isset($form_values['types'])) {
    db_query("DELETE FROM {pimport_node_type_association} WHERE pid = %d", $form_values['pid']);
    foreach ($form_values['types'] as $type) {
      db_query("INSERT INTO {pimport_node_type_association} (pid, type) VALUES (%d, '%s')", $form_values['pid'], $type);
    }
  }
  return TRUE;
} // _pimport_ui_operations_submit

/**
 * @} End of "ingroup pimport_ui".
 */