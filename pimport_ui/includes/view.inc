<?php

/**
 * @file
 * Displays the CSV Profiles overview screen at admin/settings/pimport/pimport_ui
 *
 * @ingroup pimport_ui
 * @{
 */

/**
 * Main profile management form.
 *
 * @see pimport_ui_view_submit()
 * @see theme_pimport_ui_view()
 */
function pimport_ui_view(&$form_state) {
  $header = _pimport_ui_view_header();
  $form['profiles']['#tree'] = TRUE;
  foreach (pimport_fetch_profiles(NULL, NULL, tablesort_sql($header)) as $pid => $profile) {
    $form['profiles'][$pid]['username'] = array(
      '#type' => 'markup',
      '#value' => l($profile->username, "user/{$profile->uid}"),
    );
    $form['profiles'][$pid]['title'] = array(
      '#type' => 'markup',
      '#value' => check_plain($profile->title),
    );
    $form['profiles'][$pid]['explanation'] = array(
      '#type' => 'markup',
      '#value' => check_plain($profile->explanation),
    );
  }
  if (user_access('administrate pimport module')) {
    $form['bar']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add New Profile'),
    );
  }
  return $form;
} // pimport_ui_view

/**
 * Submit callback for pimport_ui_view form.
 *
 * @see pimport_ui_view()
 */
function pimport_ui_view_submit($form, &$form_state) {
  drupal_goto('admin/settings/pimport/pimport_ui/profile/add');
} // pimport_ui_view_submit

/**
 * Themes the profile management form.
 *
 * @see pimport_ui_view()
 */
function theme_pimport_ui_view($form) {
  drupal_add_css(drupal_get_path('module', 'pimport_ui') .'/misc/pimport_ui.css');
  $header = _pimport_ui_view_header();
  foreach (element_children($form['profiles']) as $index) {
    $rows[] = array(
      drupal_render($form['profiles'][$index]['username']),
      drupal_render($form['profiles'][$index]['title']),
      drupal_render($form['profiles'][$index]['explanation']),
      l(t('Manage'), "pimport_ui/manage/$index"),
      l(t('Edit'), "admin/settings/pimport/pimport_ui/profile/edit/$index"),
      l(t('Delete'), "admin/settings/pimport/pimport_ui/profile/delete/$index"),
      l(t('Clone'), "admin/settings/pimport/pimport_ui/profile/clone/$index"),
    );
  }
  if (!empty($rows)) {
    $output = '<p>'. theme('table', $header, $rows, array('id' => 'pimport-import-table')) .'</p>';
  }
  else {
    $output = '<p>'. t("No CSV profiles have been created yet.") .'</p>';
  }

  // Render any remaining output
  $output .= drupal_render($form);

  return $output;
} // theme_pimport_ui_view

/**
 * Helper function holding the header in an array for the profile management form.
 * Avoiding code repetition and more importantly, duplicate translatable strings.
 */
function _pimport_ui_view_header() {
  return array(
    array('data' => t('Creator'), 'field' => 'u.name', 'id' => 'pimport-creator'),
    array('data' => t('Title'), 'field' => 'pip.title', 'id' => 'pimport-name', 'sort' => 'asc'),
    array('data' => t('Description'), 'id' => 'pimport-description'),
    array('data' => t('Operations'), 'colspan' => 4, 'class' => 'pimport-import-center'),
  );
} // _pimport_ui_view_header

/**
 * @} End of "ingroup pimport_ui".
 */