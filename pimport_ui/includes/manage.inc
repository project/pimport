<?php

/**
 * @file
 * Handles displaying and managing Profile configuration.
 *
 * @ingroup pimport_ui
 * @{
 */

/**
 * CSV profile management form.
 *
 * @param $form_state
 *   Drupal form_state array.
 * @param $profile
 *   CSV profile object.
 *
 * @see pimport_ui_profile_title()
 * @see pimport_ui_manage_validate()
 * @see pimport_ui_manage_submit()
 * @see theme_pimport_ui_manage()
 */
function pimport_ui_manage(&$form_state, $profile) {
  $config = pimport_ui_fetch_config($profile->pid);

  $form['title'] = array(
    '#type' => 'hidden',
    '#value' => $profile->title,
  );
  $form['explanation'] = array(
    '#type' => 'hidden',
    '#value' => $profile->explanation,
  );
  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $profile->pid,
  );
  $form['settings']['#tree'] = TRUE;
  if (!empty($config['settings']) ) {
    foreach ($config['settings'] as $setting) {
      $form['settings'][$setting->id]['heading'] = array(
        '#type' => 'textfield',
        '#default_value' => $setting->heading,
        '#size' => 24,
      );
      $form['settings'][$setting->id]['field_match'] = array(
        '#type' => 'select',
        '#options' => array_merge(array(NULL => t('-- Ignore --')), pimport_field_match_options($profile->pid, 'import')),
        '#default_value' => $setting->field_match,
      );
      $form['settings'][$setting->id]['weight'] = array(
        '#type' => 'weight',
        '#delta' => count($config['settings']),
        '#default_value' => isset($setting->weight) ? $setting->weight : 0,
      );
      $form['settings'][$setting->id]['protected'] = array(
        '#type' => 'checkbox',
        '#default_value' => $setting->protected,
      );
      $form['settings'][$setting->id]['unique_id_part'] = array(
        '#type' => 'checkbox',
        '#default_value' => $setting->unique_id_part,
      );
      drupal_alter('pimport_ui_settings', $form, $setting);
    }
    $form['bar']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
  
  $form['add'] = array(
    '#type' => 'markup',
    '#value' => l(t("Add new match"), "pimport_ui/manage/{$profile->pid}/add",
      array('attributes' => array('class' => 'import_manage_link'))
    ),
  );
  if (!empty($config['settings'])) {
    $form['export'] = array(
      '#type' => 'markup',
      '#value' => l(t("Export CSV Profile"), "pimport_ui/export_csv_profile/{$profile->pid}",
        array('attributes' => array('class' => 'import_manage_link'))
      ),
    );
  }
  $form['bar']['#theme'] = 'pimport_ui_view_bar';
  return $form;
} // pimport_ui_manage

/**
 * Validation callback for pimport_ui_manage form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_manage()
 * @see pimport_ui_manage_submit()
 */
function pimport_ui_manage_validate($form, &$form_state) {
  $unique_ID_parts = array();
  $sys_id = FALSE;
  foreach ($form_state['values']['settings'] as $id => $value) {
    $headings[$id] = $value['heading'];
    $matches[$id] = $value['field_match'];
    if ($value['unique_id_part']) {
      $unique_ID_parts[$id] = $value['heading'];
    }
    if ($value['field_match'] == "system_id") {
      $sys_id = TRUE;
    }
    // Don't let them choose a vocab as a unique ID part.
    /*
    if (preg_match("/^vocab_/", $value['field_match']) && $value['unique_id_part']) {
      $errors_uid_vocab[] = array('form_item' => "settings][$id][heading", 'field_name' => $value['heading']);
    }
    */
    if (empty($value['field_match']) && $value['unique_id_part']) {
      $errors_uid_ignore[] = array('form_item' => "settings][$id][heading", 'field_name' => $value['heading']);
    }
  }

  if ($sys_id) {
    // List errors if System ID is used and any unique ID parts are checked.
    foreach ($unique_ID_parts as $id => $value) {
      $errors_uid[] = array('form_item' => "settings][$id][heading", 'field_name' => $value);
    }
  }

  foreach ($headings as $heading) {
    $keys = array_keys($headings, $heading);
    if (count($keys) > 1) {
      foreach ($keys as $key) {
        $duplicate_headings[$key] = $headings[$key];
        unset($headings[$key]);
      }
    }
  }
  if (!empty($duplicate_headings)) {
    foreach ($duplicate_headings as $id => $heading) {
      if (!empty($heading)) {
        $errors[] = array( 'form_item' => "settings][$id][heading", 'field_name' => $heading );
      }
    }
  }

  foreach ($matches as $match) {
    $keys = array_keys($matches, $match);
    if (count($keys) > 1) {
      foreach ($keys as $key) {
        $duplicate_matches[$key] = $matches[$key];
        unset($matches[$key]);
      }
    }
  }

  if (!empty($duplicate_matches)) {
    foreach ( $duplicate_matches as $id => $match ) {
      if (!empty($match)) {
        $errors[] = array('form_item' => "settings][$id][match", 'field_name' => $match);
      }
    }
  }
  if (count($errors) || count($errors_uid)) {
    form_set_error('', t("The profile has not been saved. Please correct the problems highlighted below and click 'save'."));
  }
  if (!empty($errors)) {
    foreach ($errors as $error) {
      form_set_error($error['form_item'], t('@field is a duplicate.', array('@field' => $error['field_name'])));
    }
  }
  if (!empty($errors_uid)) {
    foreach ($errors_uid as $error) {
      form_set_error($error['form_item'], t('@field is set to Unique ID part but System generated IDs are in use.', array('@field' => $error['field_name'])));
    }
  }
  if (!empty($errors_uid_vocab)) {
    foreach ($errors_uid_vocab as $error) {
      form_set_error($error['form_item'], t('@field is set to Unique ID part but it is a Vocabulary and not supported as a UID part.', array('@field' => $error['field_name'])));
    }
  }
  if (!empty($errors_uid_ignore)) {
    foreach ($errors_uid_ignore as $error) {
      form_set_error($error['form_item'], t('@field is set to Unique ID part but it does not have a field assigned.', array('@field' => $error['field_name'])));
    }
  }
} // pimport_ui_manage_validate

/**
 * Submit callback for pimport_ui_manage form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_manage()
 * @see pimport_ui_manage_validation()
 */
function pimport_ui_manage_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  $flag = FALSE;
  foreach ($form_values['settings'] as $id => $setting) {
    $setting['id'] = $id;
    if (!(drupal_write_record('pimport_profile_data', $setting, 'id'))) {
      $flag = TRUE;
    }
  }
  if ($flag) {
    watchdog('pimport', "Error updating settings for profile '@profile'.", array('@profile' => $form_values['title']), WATCHDOG_ERROR, l(t('view'), "admin/settings/pimport/manage/". $form_values['pid']));
    drupal_set_message(t("Error updating settings for profile '@profile', please contact the <a href=\"mailto:!sysadmin\">System Administrator</a>.", array('@profile' => $form_values['title'], '!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))));
  }
  else {
    watchdog('pimport', "Settings updated for profile '@profile'.", array('@profile' => $form_values['title']), WATCHDOG_INFO, l(t('view'), "admin/settings/pimport/manage/". $form_values['pid']));
    drupal_set_message(t("Settings updated for profile '@profile'.", array('@profile' => $form_values['title'])));
  }
} // pimport_ui_manage_submit

/**
 * Pimport Profile Wizard
 * Create a CSV profile from a CSV with data or from a CSV exported by the Profile exporter.
 * Generate a profile from system fields
 *
 * @param $form_state
 *   Drupal form_state array.
 * @param $profile
 *   pimport CSV profile object.
 *
 * @see pimport_ui_manage_import_validate()
 * @see pimport_ui_manage_import_submit()
 */
function pimport_ui_manage_import(&$form_state, $profile) {
  global $user;
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  
  $form['fields_from_types_submit'] = array(
    '#title' => t('Add fields and headings from content types'),
    '#type' => 'submit',
    '#value' => t('Add'),
    '#prefix' => t('Add fields and headings from content types') .'<br/>',
    '#suffix' => '<br/>'. t('Add all the fields from associated content types, with their labels (human readable names)
      as headings. If no content type is associated with this profile, no cck fields will be added.'),
  );
  
  $form['upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload'),
    '#description' => t('Click "Browse" to select a csv file for processing.'),
  );
  // Check to see if $user has the administrator role.
  if (in_array('administrative user', array_values($user->roles))) {
    $form['field_names'] = array(
      '#type' => 'checkbox',
      '#title' => t('pimport exported profile'),
      '#description' => t('Check this if the CSV has headings on the first row, CCK field names on the second row,
        protected info on the third row, unique id info on the fourth row (i.e. was exported by MEAT CSV profile exporter).
        If this is not checked, just the headings will be imported from the first row and CCK field names must be manually
        matched in the CSV Profile Manage screen.'),
    );
  }
  else {
    $form['field_names'] = array(
      '#type' => 'hidden',
      '#value' => FALSE,
    );
  }

  $form['bar']['#theme'] = 'pimport_ui_view_bar';
  $form['bar']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );
  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $profile->pid,
  );
  $form['file_placeholder'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  return $form;
} // pimport_ui_manage_import

/**
 * Validation callback for pimport_ui_manage_import form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_manage_import()
 * @see pimport_ui_manage_import_submit()
 */
function pimport_ui_manage_import_validate(&$form, &$form_state) {
  switch ($form_state['clicked_button']['#value']) {
    case t('Upload'):
      $validators = array(
        'pimport_is_spreadsheet' => array(),
      );
      if (!($file = file_save_upload('upload', $validators))) {
        // return an error on failure
        form_set_error('upload', t('File upload failed, please contact your <a href="mailto:!sysadmin">System Administrator</a>.', array('!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))));
      }
      else {
        // fill in placeholder variable so we get to pass our file variables into the submit function
        form_set_value(array('#parents' => array('file_placeholder')), $file, $form_state);
      }
      break;
  } // end switch
} // pimport_ui_manage_import_validate

/**
 * Submit callback for pimport_ui_manage_import form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_manage_import()
 * @see pimport_ui_manage_import_validate()
 */
function pimport_ui_manage_import_submit($form, &$form_state) {
 
  $pid = $form_state['values']['pid'];
  $file = $form_state['values']['file_placeholder'];
  if ($form_state['clicked_button']['#value'] == t('Add')) {
    $matches = _pimport_ui_manage_add_fields_from_types($pid);
  }
  else {
    $matches = _pimport_ui_manage_add_fields_from_file($file, $form_state['values']['field_names']);
  }
  // process...
  $weight = 0;
  foreach ($matches as $header => $value) {
    if (db_result(db_query("SELECT heading FROM {pimport_profile_data} WHERE heading = '%s' && pid = %d", $header, $pid))) {
      drupal_set_message(t("Error duplicate heading: '@header'", array('@header' => $header)), 'error');
    }
    else {
      db_query("INSERT INTO {pimport_profile_data} (pid, heading, field_match, weight, protected, unique_id_part) 
        VALUES (%d, '%s', '%s', %d, %d, %d)", 
        $pid, $header, $value['field_match'], $weight, $value['protected'], $value['unique_id_part']);
    }
    $weight += 3;
  }
  $form_state['redirect'] = "pimport_ui/manage/$pid";
} // pimport_ui_manage_import_submit

/**
 * Wizard to add all the fields to a CSV profile in the associated content types
 * If no types are associated all system fields will be added
 *
 * @param unknown_type $pid
 * @return unknown
 */
function _pimport_ui_manage_add_fields_from_types($pid) {
  $matches = array();
  // we can't use the human name of title and body because they could be different across types
  $matches['nid'] = array('field_match' => 'nid', 'unique_id_part' => TRUE);
  $matches['title'] = array('field_match' => 'title');
  $matches['body'] = array('field_match' => 'body');
  $types = pimport_profile_fetch_types($pid);
  $fields = array();
  $vocabs = array();
  if (empty($types)) {
    drupal_set_message(t('No content type is associated with this profile so no fields have been added.'), 'warning');
  }
  else {
    foreach ($types as $type) {
      $fields = array_merge($fields, pimport_content_fields(NULL, $type));
      $vocabs = array_merge(taxonomy_get_vocabularies($type));
    }
  }
  // already sorted?
  // uasort($fields, '_meat_import_compare_fields');
  foreach ($fields as $field) {
    $matches[$field['label']] = array('field_match' => $field['field_name']);
  }
  foreach ($vocabs as $vocab) {
    $matches[$vocab->name] = array('field_match' => "vocab_". $vocab->vid);
  }
  
  if (empty($fields)) {
    drupal_set_message(t("No fields or vocabs found"), 'warning');
  }
  return $matches;
}

/**
 * Compare fields callback for uasort
 *
 * @param unknown_type $a
 * @param unknown_type $b
 * @return unknown
 */
function _pimport_compare_fields($a, $b) {
  if ($a['widget']['weight'] == $b['widget']['weight']) {
    return 0;
  }
  return $a['widget']['weight'] > $b['widget']['weight'] ? 1 : -1;
}
/**
 * Import a CSV profile or headings in CSV
 *
 * @param unknown_type $pid
 * @param unknown_type $file
 */
function _pimport_ui_manage_add_fields_from_file($file, $is_profile = FALSE) {
// create a schema based on file header...
  $matches = array();
  $handle = fopen($file->filepath, "r");
  $headers = fgetcsv($handle);
  $fieldnames = array();
  if ($is_profile) {
    $fieldnames = fgetcsv($handle);
    $protected = fgetcsv($handle);
    $unique_id_part = fgetcsv($handle);
  }
  foreach ($headers as $key => $value) {
    $matches[$value]['field_match'] = !empty($fieldnames) ? $fieldnames[$key] : "";
    $matches[$value]['protected'] = $protected[$key];
    $matches[$value]['unique_id_part'] = $unique_id_part[$key];
  }
  // clean up
  fclose($handle);
  file_delete($file->filepath);
  db_query("DELETE FROM {files} WHERE fid = %d", $file->fid);
  return $matches;
}
  
/**
 * Add field match to CSV profile.
 *
 * @param $form_state
 *   Drupal form_state array.
 * @param $profile
 *   pimport CSV profile object.
 *
 * @see pimport_ui_manage_add_validate()
 * @see pimport_ui_manage_add_submit()
 */
function pimport_ui_manage_add(&$form_state, $profile) {
  $form['title'] = array(
    '#type' => 'hidden',
    '#value' => $profile->title,
  );
  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $profile->pid,
  );
  $form['heading'] = array(
    '#type' => 'textfield',
    '#title' => t('Column'),
  );
  $form['field_match'] = array(
    '#type' => 'select',
    '#title' => t('Field match'),
    '#options' => array_merge(array(NULL => t('-- Ignore --')), pimport_field_match_options($profile->pid, 'import')),
  );
  $form['protected'] = array(
    '#type' => 'checkbox',
    '#title' => t('Protected field'),
  );
  $form['unique_id_part'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unique ID part'),
  );
  return confirm_form($form, t("Add new match to '@profile'?", array('@profile' => $profile->title)), "pimport_ui/manage/{$profile->pid}", ' ');
} // pimport_ui_manage_add

/**
 * Validation callback for pimport_ui_manage_add form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_manage_add()
 * @see pimport_ui_manage_add_submit()
 */
function pimport_ui_manage_add_validate($form, &$form_state) {
  switch ($form_state['clicked_button']['#value']) {
    case t('Confirm'):
      if (empty($form_state['values']['heading']) && empty($form_state['values']['field_match'])) {
        form_set_error( 'heading', t("You should supply a heading and/or a match.") );
      }
      // Check for existence of this match in the profile if it's not blank.
      if (!empty($form_state['values']['field_match'])) {
        if (db_result(db_query("SELECT 1 FROM {pimport_profile_data} WHERE pid = %d && field_match = '%s'", $form_state['values']['pid'], $form_state['values']['field_match']))) {
          form_set_error('field_match', t("The match '@match' already exists in the profile. You can use a different match or use the management form to edit the heading or delete the match.", array('@match' => $form_state['values']['field_match'])));
        }
      }
      // Same check for heading.
      if (!empty($form_state['values']['heading'])) {
        if (db_result(db_query("SELECT 1 FROM {pimport_profile_data} WHERE pid = %d && heading = '%s'", $form_state['values']['pid'], $form_state['values']['heading']))) form_set_error('heading', t("The heading '@heading' already exists in the profile. You can use a different heading or use the management form to edit the heading or delete the match.", array('@heading' => $form_state['values']['heading'])));
      }
      break;
  } // end switch
} // pimport_ui_manage_add_validate

/**
 * Submit callback for pimport_ui_manage_add form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_manage_add_validate()
 * @see pimport_ui_manage_add_submit()
 */
function pimport_ui_manage_add_submit($form, &$form_state) {
  if (drupal_write_record('pimport_profile_data', $form_state['values'])) {
      watchdog('pimport', "Profile field '@field' = '@match' successfully added to '@profile'.", array('@field' => $form_state['values']['heading'], '@match' => $form_state['values']['field_match'], '@profile' => $form_state['values']['title']), WATCHDOG_INFO);
      drupal_set_message(t("Profile field '@field' = '@match' successfully added to '@profile'.", array('@field' => $form_state['values']['heading'], '@match' => $form_state['values']['field_match'], '@profile' => $form_state['values']['title'])));
  }
  else {
    watchdog('pimport', "Error adding field '@field' to '@profile'.", array('@field' => $form_state['values']['heading'], '@profile' => $form_state['values']['title']), WATCHDOG_ERROR, l(t('view'), "admin/settings/pimport/manage/". $form_state['values']['pid']));
    drupal_set_message(t("Error adding field '@field' to '@profile', please contact the <a href=\"mailto:!sysadmin\">System Administrator</a>.", array('@field' => $form_state['values']['heading'], '@profile' => $form_state['values']['title'], '!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))), 'error');
  }
  $form_state['redirect'] = "pimport_ui/manage/{$form_state['values']['pid']}";
} // pimport_ui_manage_add_submit

/**
 * Form for deleting a field match from a profile.
 *
 * @param $form_state
 *   Drupal form_state array.
 * @param $profile
 *   pimport CSV profile object.
 * @param $field
 *   pimport CSV profile field object.
 *
 * @see pimport_ui_manage_delete_submit()
 * @see theme_pimport_display_field()
 */
function pimport_ui_manage_delete(&$form_state, $profile, $field) {
  $form['display'] = array(
    '#type' => 'markup',
    '#value' => theme('pimport_display_field', $field),
  );
  $form['title'] = array(
    '#type' => 'hidden',
    '#value' => $profile->title,
  );
  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $profile->pid,
  );
  $form['heading'] = array(
    '#type' => 'hidden',
    '#value' => $field->heading,
  );
  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $field->id,
  );
  return confirm_form($form, t("Delete field '@field' from '@profile'?", array('@field' => $field->heading,  '@profile' => $profile->title)), "pimport_ui/manage/{$profile->pid}", t("<p>Caution this action is permanent and cannot be undone.</p>"));
} // pimport_ui_manage_delete

/**
 * Submit callback for pimport_ui_manage_delete form.
 *
 * @param $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form_state array.
 *
 * @see pimport_ui_manage_delete()
 */
function pimport_ui_manage_delete_submit($form, &$form_state) {
  if (db_query("DELETE FROM {pimport_profile_data} WHERE id = %d", $form_state['values']['id'])) {
    watchdog('pimport', "Profile field '@field' successfully deleted from '@profile'.", array('@field' => $form_state['values']['heading'], '@profile' => $form_state['values']['title']), WATCHDOG_INFO);
    drupal_set_message(t("Profile field '@field' successfully deleted from '@profile'.", array('@field' => $form_state['values']['heading'], '@profile' => $form_state['values']['title'])));
  }
  else {
    watchdog('pimport', "Error deleting field '@field' from '@profile'.", array('@field' => $form_state['values']['heading'], '@profile' => $form_state['values']['title']), WATCHDOG_ERROR, l(t('view'), "admin/settings/pimport/manage/". $form_state['values']['pid']));
    drupal_set_message(t("Error deleting field '@field' from '@profile', please contact the <a href=\"mailto:!sysadmin\">System Administrator</a>.", array('@field' => $form_state['values']['heading'], '@profile' => $form_state['values']['title'], '!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))), 'error');
  }
  $form_state['redirect'] = "pimport_ui/manage/{$form_state['values']['pid']}";
} // pimport_ui_manage_delete_submit

/**
 * Themes the management form.
 *
 * @see pimport_ui_manage()
 */
function theme_pimport_ui_manage($form) {
  drupal_add_css(drupal_get_path('module', 'pimport_ui') .'/misc/pimport_ui.css');
  $pid = $form['pid']['#value'];
  drupal_add_tabledrag('pimport-import-table', 'order', 'self', 'settings-weight');
  $header = array(
    t('Column Heading'),
    t('Field Match'),
    t('Column Weight'),
    t('Protected'),
    t('Unique ID pt'),
  );
  drupal_alter('pimport_ui_header', $header);
  $header[] = array('data' => t('Operations'), 'colspan' => 2, 'class' => 'pimport-import-center');

  foreach (element_children($form['settings']) as $index) {
    $form['settings'][$index]['weight']['#attributes']['class'] = 'settings-weight';
    $row = array(
      drupal_render($form['settings'][$index]['heading']),
      drupal_render($form['settings'][$index]['field_match']),
      drupal_render($form['settings'][$index]['weight']),
      drupal_render($form['settings'][$index]['protected']),
      drupal_render($form['settings'][$index]['unique_id_part']),
    );
    $data = &$form;
    $data['__drupal_alter_by_ref'] = array(&$row);
    drupal_alter('pimport_ui_row', $data, $index);
    $row[] = l(t('Delete'), "pimport_ui/manage/{$pid}/delete/{$index}");
    $rows[$index] = array(
      'data' => $row,
      'class' => 'draggable',
    );
  }
  if (!empty($rows)) {
    $output = '<p>'. theme('table', $header, $rows, array('id' => 'pimport-import-table'), $form['explanation']['#value']) .'</p>';
  }
  else {
    $output = l(t('Use profile creation wizard'), "pimport_ui/manage/{$pid}/import", array('query' => drupal_get_destination()));
  }

  // Render any remaining output
  $output .= drupal_render($form);

  return $output;
} // theme_pimport_ui_manage

/**
 * @} End of "ingroup pimport_ui".
 */