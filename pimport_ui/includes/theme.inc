<?php

/**
 * @file
 * 
 *
 * @ingroup pimport_ui
 * @{
 */

//----------------------------------------------------------------------------
// Theme callbacks.

/**
 * Displays a single profile in a table.
 *
 * @param $profile
 *   pimport CSV profile object.
 *
 * @see pimport_ui_delete()
 */
function theme_pimport_display($profile) {
  drupal_add_css(drupal_get_path('module', 'pimport_ui') .'/misc/pimport_ui.css');
  if (!empty($profile)) {
    $rows[] = array(
      $profile->title,
    );
  }
  if (!empty($rows)) {
    $header = array(
      t('Title'),
    );
    $output = '<p>'. theme('table', $header, $rows, array('id' => 'pimport-import-table')) .'</p>';
  }
  else {
    $output = '<p>'. t('Error displaying profile, please contact your <a href="mailto:!sysadmin">System Administrator</a>.', array('!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))) .'</p>';
  }
  return $output;
} // theme_pimport_display

/**
 * Themes a single pimport CSV profile field (so a user can view what they're deleting).
 *
 * @param $field
 *   pimport CSV profile field object.
 *
 * @see pimport_ui_manage_delete()
 */
function theme_pimport_display_field($field) {
  drupal_add_css(drupal_get_path('module', 'pimport_ui') .'/misc/pimport_ui.css');
  if (!empty($field)) {
    $rows[] = array(
      $field->heading,
      $field->field_match,
    );
  }
  if (!empty($rows)) {
    $header = array(
      t('Column Heading'),
      t('CCK Field Match'),
    );
    $output = '<p>'. theme('table', $header, $rows, array('id' => 'pimport-import-table')) .'</p>';
  }
  else {
    $output = '<p>'. t('Error displaying field, please contact your <a href="mailto:!sysadmin">System Administrator</a>.', array('!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))) .'</p>';
  }
  return $output;
} // theme_pimport_display_field

/**
 * Theme the submission buttons along the bottom of the profile management form.
 */
function theme_pimport_ui_view_bar($form) {
  $output = '<div class="container-inline">';
  $output .= drupal_render($form['submit']);
  $output .= '</div>';
  // Render any remaining form elements
  $output .= drupal_render($form);
  $output .= l(t('Return to profile overview screen'), 'admin/settings/pimport/pimport_ui');
  return $output;
} // theme_pimport_ui_view_bar

/**
 * @} End of "ingroup pimport_ui".
 */