<?php

/**
 * @file
 * Core Pimport settings file. Sub-modules can use hook_pimport_settings() to merge
 * extra settings with this form.
 *
 * @ingroup pimport_core
 * @{
 */

/**
 * Administration page.
 */
function pimport_settings() {
  $form['pimport'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'additional_settings',
  );
  $form['pimport']['pimport_content_types'] = array(
    '#type' => 'select',
    '#title' => t('Enable for these Content-types'),
    '#description' => t('Select which content types pimport should interact with.'),
    '#default_value' => variable_get('pimport_content_types', array()),
    '#options' => array_map('filter_xss', node_get_types('names')),
    '#multiple' => TRUE,
  );
  $form['pimport']['pimport_use_permissions'] = array(
    '#type' => 'checkbox',
    '#title' => t("Use permissions"),
    '#description' => t('To add to the confusion, checking this box will activate <a href="@url">permissions</a> PER PROFILE.<br />The downside to this is that if a user creates a profile they *must* also be able to grant permissions else they won\'t be able to view their own profile.', array('@url' => url('admin/user/permissions', array('fragment' => 'module-pimport')))),
    '#default_value' => variable_get('pimport_use_permissions', FALSE),
  );
  /*
  // Isn't this an XML setting? Can't find it here.
  $form['pimport']['pimport_force_download'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force downloads'),
    '#description' => t("Check this to provide <strong>an option</strong> to download generated XML files even if they fail validation."),
    '#default_value' => variable_get('pimport_force_download', FALSE),
  );
  */
  $form['pimport']['pimport_csv_encoding'] = array(
    '#type' => 'radios',
    '#title' => t('CSV Character encoding'),
    '#description' => t('Select the character encoding of the text in the CSV for imports and exports.'),
    '#options' => array(
      'UTF-8' => 'UTF-8 (Standards compliant editor or database export)',
      'Windows-1252' => 'Windows-1252 (Microsoft Excel default)',
    ),
    '#default_value' => variable_get('pimport_csv_encoding', 'UTF-8'),
  );
  $form['pimport_import_defaults'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Defaults'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
  );
  $form['pimport_import_defaults']['pimport_default_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Default Content Type'),
    '#description' => t("If it's not set per user, choose which content type will be pre-selected on the import screen."),
    '#options' => array(NULL => t('-- None --')) + _pimport_content_type_options(),
    '#default_value' => variable_get('pimport_default_content_type', NULL),
  );
  $form['pimport_import_defaults']['pimport_default_profile'] = array(
    '#type' => 'select',
    '#title' => t('Default Profile'),
    '#description' => t("If it's not set per user, choose which profile will be pre-selected on the import screen."),
    '#options' => array(NULL => t('-- None --')) + pimport_fetch_profiles(NULL, 'names'),
    '#default_value' => variable_get('pimport_default_profile', NULL),
  );
  $form['pimport_import_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
  );
  $form['pimport_import_settings']['pimport_empty_fields'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overwrite when empty'),
    '#description' => t("The default behaviour is to ignore a blank input field in CSV during import - check this to overwrite the field WITH an empty value if one is given."),
    '#default_value' => variable_get('pimport_empty_fields', FALSE),
  );
  $form['pimport_import_settings']['pimport_batch_process_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of rows to process'),
    '#size' => 12,
    '#description' => t("The batch import process can time out if too many rows are set to process, only change the default of 25 if you're absolutely sure of what you're doing."),
    '#default_value' => variable_get("pimport_batch_process_limit", 25),
  );
  $form['pimport_import_settings']['pimport_error_handling'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import behaviour on error settings'),
    '#tree' => TRUE,
  );
  $error_handling_options =  array(t('Fail'), t('Warn'), t('Ignore'));
  $error_handling_default_values = variable_get("pimport_error_handling", array());
  $form['pimport_import_settings']['pimport_error_handling']['taxonomy'] = array(
    '#type' => 'radios',
    '#title' => t('Import behaviour on taxonomy errors'),
    '#default_value' => isset($error_handling_default_values['taxonomy']) ? 
      $error_handling_default_values['taxonomy'] : PIMPORT_ERROR_WARN,
    '#options' => $error_handling_options,
    '#description' => t('If there is an error in taxonomy data supplied by the import should the import 
    	<strong>Fail</strong> to create or update the node, 
    	<strong>Warn</strong> but still create or update the node or 
    	<strong>Ignore</strong> the error and create or update the node anyway.'),
  );
  $form['pimport_import_settings']['pimport_error_handling']['language'] = array(
    '#type' => 'radios',
    '#title' => t('Import behaviour on language errors'),
    '#default_value' => isset($error_handling_default_values['language']) ? 
      $error_handling_default_values['language'] : PIMPORT_ERROR_WARN,
    '#options' => $error_handling_options,
    '#description' => t('If there is an error in language data supplied by the import should the import 
    	<strong>Fail</strong> to create or update the node, 
    	<strong>Warn</strong> but still create or update the node or 
    	<strong>Ignore</strong> the error and create or update the node anyway.'),
  );
  $form['pimport_import_settings']['pimport_error_handling']['other'] = array(
    '#type' => 'radios',
    '#title' => t('Import behaviour on other errors'),
    '#default_value' => isset($error_handling_default_values['other']) ? 
      $error_handling_default_values['other'] : PIMPORT_ERROR_FAIL,
    '#options' => $error_handling_options,
    '#description' => t('If there is an error in data supplied by the import (other than the cases above) should the import 
    	<strong>Fail</strong> to create or update the node, 
    	<strong>Warn</strong> but still create or update the node or 
    	<strong>Ignore</strong> the error and create or update the node anyway.'),
  );
  $form['pimport_date_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Date Field Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
  );
  $form['pimport_date_settings']['pimport_date_region'] = array(
    '#type' => 'select',
    '#title' => t('CSV Date format'),
    '#description' => t("If your import spreadsheet contains European formatted dates (e.g. d/m/y) then select 'European' else select 'American' (e.g. m/d/y."),
    '#options' => pimport_date_formats(NULL, FALSE),
    '#default_value' => variable_get('pimport_date_region', 'eu'),
  );
  $form['pimport_date_settings']['pimport_date_output'] = array(
    '#type' => 'textfield',
    '#title' => t('Date Format into CCK'),
    '#size' => 12,
    '#description' => t("<strong>NOTE:</strong> It is highly unlikely you'll need to change this setting. Understand what it does before playing with it.
      Date format for import *into* Drupal CCK date fields, for reference please see; <a href=\"@url\">@url</a>.", array('@url' => url("http://www.php.net/date"))),
    '#default_value' => variable_get('pimport_date_output', 'Y-m-d H:i:s'),
  );

  $form += module_invoke_all('pimport_settings');
  $form['#pre_render'][] = 'vertical_tabs_form_pre_render';

  $form = system_settings_form($form);
  $form['buttons']['#weight'] = 100;
  return $form;
} // pimport_settings

/**
 * @} End of "ingroup pimport_core".
 */