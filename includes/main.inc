<?php

/**
 * @file
 * Import configuration include file for the pimport module.
 *
 * @ingroup pimport_core
 * @{
 */

/**
 * Import form.
 *
 * @see pimport_import_validate()
 * @see pimport_import_submit()
 * @see theme_pimport_import()
 */
function pimport_import() {
  if (count(variable_get('pimport_content_types', array())) == 0 ) {
    drupal_set_message(t('Error: No content types have been enabled to work with imports. Please contact your <a href="mailto:!sysadmin">System Administrator</a>.', array('!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))), 'error');
    return;
  }
  if (count(pimport_fetch_profiles()) == 0) {
    drupal_set_message(t('Error: No CSV profiles have been created. Please contact your <a href="mailto:!sysadmin">System Administrator</a>.', array('!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))), 'error');
    return;
  }
  global $user;
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form['content-type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#description' => t('Select which content-type your spreadsheet will be imported into.'),
    '#options' => _pimport_content_type_options(),
    '#default_value' => (user_access('collect user import defaults') && !empty($user->pimport_default_content_type)) ? $user->pimport_default_content_type : variable_get('pimport_default_content_type', NULL),
  );
  $form['profile'] = array(
    '#type' => 'select',
    '#title' => t('Profile'),
    '#description' => t('Select which profile to use for this import.'),
    '#options' => pimport_fetch_profiles(NULL, 'names'),
    '#default_value' => (user_access('collect user import defaults') && !empty($user->pimport_default_profile)) ? $user->pimport_default_profile : variable_get('pimport_default_profile', NULL),
  );
  $form['upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload'),
    '#description' => t('Click "Browse" to select a csv file for processing.'),
  );
  $form['bar']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Process'),
  );
  if ($user->uid == 1 || user_access('administrate pimport module')) {
    $form['bar']['test_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Test Process 1 Row'),
    );
  }
  $form['file_placeholder'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  return $form;
} // pimport_import

/**
 * Validation callback for the pimport_import form.
 *
 * @see pimport_import()
 * @see pimport_import_submit()
 */
function pimport_import_validate(&$form, &$form_state) {
	$form_values = $form_state['values'];
	if (!db_result(db_query("SELECT 1 FROM {pimport_node_type_association} WHERE pid = %d && type = '%s'", $form_values['profile'], $form_values['content-type']))) {
		form_set_error('profile', t('The selected profile is not associated with the selected content-type. If you feel you have received this message in error, please contact your <a href="mailto:!sysadmin">System Administrator</a>.', array('!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))));
	}

  switch ($form_state['clicked_button']['#value']) {
    case t('Process'):
    case t('Test Process 1 Row'):
      $validators = array(
        'pimport_is_spreadsheet' => array(),
      );
      if (!($file = file_save_upload('upload', $validators))) {
        // return an error on failure
        form_set_error('upload', t('File upload failed, please contact your <a href="mailto:!sysadmin">System Administrator</a>.', array('!sysadmin' => variable_get('site_mail', ini_get('sendmail_from')))));
      }
      else {
        // fill in placeholder variable so we get to pass our file variables into the submit function
        form_set_value(array('#parents' => array('file_placeholder')), $file, $form_state);
      }
      break;
  } // end switch
} // pimport_import_validate

/**
 * Submit callback for the pimport_import form.
 *
 * @see pimport_import()
 * @see pimport_import_validate()
 */
function pimport_import_submit($form, &$form_state) {
  global $user;
  $form_values = $form_state['values'];
  switch ($form_state['clicked_button']['#value']) {
    case t('Process'):
    $batch = array(
      'operations' => array(
        array(
          'pimport_batch_process',
            array(
              $user,
              $form_values['content-type'],
              $form_values['profile'],
              $form_values['file_placeholder'],
              variable_get('pimport_csv_encoding', 'Windows-1252'),
            ),
          ),
        ),
      'finished' => 'pimport_batch_finished',
      'title' => t('Processing import...'),
      'init_message' => t('Batch processing is starting...'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Batch processing has encountered an error.'),
    );
    batch_set($batch);
    break;
    case t('Test Process 1 Row'):      
      $context = array('sandbox' => array(), 'message' => array(), 'results' => array());
      $results = pimport_batch_process(
        $user,
        $form_values['content-type'],
        $form_values['profile'],
        $form_values['file_placeholder'],
        variable_get('pimport_csv_encoding', 'Windows-1252'),
        $context,
        TRUE
      );
      if (is_array($results['processed'])) {
        foreach ($results['processed'] as $msg) {
          drupal_set_message($msg);
        }
      }
      if (is_array($results['error'])) {
        foreach ($results['error'] as $err) {      
          drupal_set_message($err, 'error');
        }
      }
      break;
  }
} // pimport_import_submit

/**
 * Theme function for the pimport_import form.
 *
 * @see pimport_import()
 */
function theme_pimport_import($form) {
  drupal_add_css(drupal_get_path('module', 'pimport') .'/misc/admin.css');
  global $user;
  $link = '';
  // Render any remaining form elements.
  $output = drupal_render($form);
  if ($user->uid == 1 || user_access('administrate pimport module')) {
    $link = t('To change this setting, please click through to the <a href="@link">settings page</a>.', array('@link' => url('admin/settings/pimport')));
  }
  $output .= '<div id="pimport-encoding-message">'. t('<strong>Info:</strong> Your CSV encoding type is currently set to [@type].', array('@type' => variable_get('pimport_csv_encoding', 'Windows-1252'))) .'<p>'. $link .'</p></div>';
  return $output;
} // theme_pimport_import

/**
 * @} End of "ingroup pimport_core".
 */
