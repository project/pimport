<?php

/**
 * @file
 * Installation file for Pimport project.
 *
 * @ingroup pimport_core
 * @{
 */

/**
 * Implementation of hook_install().
 */
function pimport_install() {
  drupal_install_schema('pimport');
} // pimport_install

/**
 * Implementation of hook_uninstall().
 */
function pimport_uninstall() {
  drupal_uninstall_schema('pimport');
  variable_del('pimport_content_types');
  variable_del('pimport_use_permissions');
  variable_del('pimport_force_download');
  variable_del('pimport_csv_encoding');
  variable_del('pimport_default_content_type');
  variable_del('pimport_default_profile');
  variable_del('pimport_empty_fields');
  variable_del('pimport_batch_process_limit');
  variable_del('pimport_date_region');
  variable_del('pimport_date_output');
  variable_del('pimport_error_handling');
} // pimport_uninstall

/**
 * Implementation of hook_schema().
 */
function pimport_schema() {
  $schema['pimport_profiles'] = array(
    'description' => 'Table containing pimport CSV profile information.',
    'fields' => array(
      'pid' => array(
        'description' => 'Unique profile identifier.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The {users}.uid who owns this profile; initially, this is the user who created it.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'title' => array(
        'description' => 'The human readable title of this profile always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'explanation' => array(
        'description' => 'A short description of this profile, always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'settings' => array(
        'description' => 'If the type needs to hold any extra settings, these are stored here.',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'count' => array(
        'description' => 'The number of times this profile has been used to import metadata.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the profile was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the profile was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('pid'),
  );
  $schema['pimport_node_type_association'] = array(
    'description' => 'Table containing pimport CSV profile node type associations.',
    'fields' => array(
      'pid' => array(
        'description' => '{pimport_profiles}.pid reference.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'type' => array(
        'description' => 'The {node_type}.type association for this profile.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('pid', 'type'),
  );
  $schema['pimport_profile_data'] = array(
    'description' => 'Table containing all pimport CSV profile data.',
    'fields' => array(
      'id' => array(
        'description' => 'Unique field identifier.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'pid' => array(
        'description' => '{pimport_profiles}.pid reference.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'heading' => array(
        'description' => 'Spreadsheet column reference.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'field_match' => array(
        'description' => 'Field match reference.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'description' => 'Field weight',
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'protected' => array(
        'description' => 'This field is protected against alteration in future revisions',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'unique_id_part' => array(
        'description' => 'This field forms part of the unique ID',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),

    ),
    'primary key' => array('id'),
  );
  return $schema;
} // pimport_schema

/**
 * @} End of "ingroup pimport_core".
 */