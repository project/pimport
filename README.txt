
-- SUMMARY --

Pimport imports and exports nodes in CSV format. 

Features:
Unique ID field(s)
User allocation
CCK multiple values
Locale (and i18n) integration
Supports UTF-8 and Windows-1252 (Excel)

Fields are mapped to columns in a CSV by headings, which are expected to be in the first row of the CSV file. 
Mappings are called profiles and are created using Pimport UI.  Profiles can be generated from a CSV file, exported
and imported by the module.

-- EXAMPLE USE CASE --

Job Allocation

You have a spreadsheet for job allocation with various features of a job - ID number, start date, due date, workers name, 
description, etc. Your company currently uses the spreadsheet to keep track of who is doing what and when. 
Set up a content type with the relevant fields and a profile for Pimport, choose ID number as your unique ID field and map 
workers name to user name. Import the spreadsheet - nodes are created for each row and your workers can log in to see what
they have to do.  
Make changes in the Drupal UI, export your nodes back to CSV to show the boss. 
Make changes to the CSV and re-import - Pimport recognises the nodes from the ID number and creates new revisions with the new 
data.

For a full description of the module, visit the project page:
  http://drupal.org/project/pimport

For documentation of the module, visit the handbook pages:
  http://drupal.org/node/xxxxx

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/pimport

-- REQUIRED --

N/A

-- OPTIONAL --

transliteration - http://drupal.org/project/transliteration
vertical_tabs - http://drupal.org/project/vertical_tabs

-- MAINTAINERS --

pobster - http://drupal.org/user/25159
rayvaughn - http://drupal.org/user/190002


